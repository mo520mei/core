CORE（Common Open Research Emulator）是github上一个基于linux namespace技术的网络仿真平台，同时它还集成了EMANE、OSPF等功能，也留出了开发的接口留给开发者可以去开发自己需要的功能。

这个存储库是我在CORE的基础之上重新开发了OVS服务模块，解决了命名空间之间的进程和网桥隔离问题，增强了OVS的扩展性。

说明文档：
* 我的说明文档：[core-note](https://gitee.com/pengfeim/core-note)
* 原版说明文档：[Core-Documentation](https://coreemu.github.io/core/)



**以下为CORE原有说明**
# CORE

CORE: Common Open Research Emulator

Copyright (c)2005-2020 the Boeing Company.

See the LICENSE file included in this distribution.

## About

The Common Open Research Emulator (CORE) is a tool for emulating
networks on one or more machines. You can connect these emulated
networks to live networks. CORE consists of a GUI for drawing
topologies of lightweight virtual machines, and Python modules for
scripting network emulation.

## Documentation & Support

We are leveraging GitHub hosted documentation and Discord for persistent
chat rooms. This allows for more dynamic conversations and the
capability to respond faster. Feel free to join us at the link below.

* [Documentation](https://coreemu.github.io/core/)
* [Discord Channel](https://discord.gg/AKd7kmP)
